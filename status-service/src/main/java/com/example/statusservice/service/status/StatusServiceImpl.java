package com.example.statusservice.service.status;

import com.example.statusservice.dto.StatusResponseDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@AllArgsConstructor
@Service
public class StatusServiceImpl implements IStatusService {
    @Override
    public void changeStatus(StatusResponseDto statusResponseDto, Integer deviceId) {
        log.info("Do something with status: {} and deviceId: {}", statusResponseDto.getStatus(), deviceId);
    }
}
