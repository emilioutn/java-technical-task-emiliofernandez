package com.example.statusservice.service.status;

import com.example.statusservice.dto.StatusResponseDto;

public interface IStatusService {
    public void changeStatus(StatusResponseDto statusResponseDto, Integer deviceId);
}
