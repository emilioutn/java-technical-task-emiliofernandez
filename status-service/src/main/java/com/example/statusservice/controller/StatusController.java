package com.example.statusservice.controller;

import com.example.statusservice.dto.StatusResponseDto;
import com.example.statusservice.service.status.IStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatusController {
    @Autowired
    private IStatusService statusService;

    @PostMapping(value = "/devices/{id}", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public void notifyUser(@RequestBody StatusResponseDto statusResponseDto, @PathVariable Integer id) {
        statusService.changeStatus(statusResponseDto, id);
    }
}
