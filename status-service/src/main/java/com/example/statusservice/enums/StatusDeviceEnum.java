package com.example.statusservice.enums;

public enum StatusDeviceEnum {
    UP,
    DOWN,
    ALERTED
}
