# kafka-spring-rest

Proyect with Kafka REST, Mongo DB and MySql - Feign and Eureka.

These commands were made in the Windows Operative System.

## TODO in Kafka:

- [ ] Start zookeeper in a bash:

- [ ] Start any kafka server, for examplple (cluster for default and in 9092 port): 

- [ ] Create a defined topic (whatever it is):

```
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 3 --topic external-topic
```

### Note: The name of the topic es "external-topic". This name is configured in the Spring microservice

- [ ] Create the "push notification" topic

```
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 3 --topic push-notifications
```

- [ ] Create a group to veiew the push notification (in this window you will see the end point: Receive push notification for an external consumer)

```
kafka-console-consumer --bootstrap-server localhost:9092 --group group_1 --topic push-notifications --from-beginning
```

### Note: This windows will allow you to view all push notifications made on the device-service service

***

## Create Producer for start stream processing in the device-service service, after startup

```
kafka-console-producer --broker-list localhost:9092 --topic external-topic
```

### To get started you can use this message or similar:

```
{'id': 1, 'status': 'ALERTED'}
```

***

# To start the test, you need to start these projects, in the following order:

```
1 - eureka-server
2 - status-service
3 - device-service
```

- [ ] Next you must start the local zookeeper server and the local kafka server with the steps explained at the beginning of this document

- [ ] Finally you should be able to look at the consoles and see the change. You could see it before to compare.

```
1 - Enter into H2 from: 
	http://localhost:8080/h2-console
	
    whit this credentials: 
	
	Username: root
	password: root
```	

- [ ]  Check your local mongodb (you must have a mongodb installed on your computer) for the new message received.
- [ ]  Check in your "consumer push" console for push notification.
- [ ]  check the log of services. 

