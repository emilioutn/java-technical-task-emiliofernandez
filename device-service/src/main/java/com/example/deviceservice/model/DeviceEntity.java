package com.example.deviceservice.model;

import com.example.deviceservice.enums.StatusDeviceEnum;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "device")
public class DeviceEntity {
    @Id
    private Integer id;

    @NotNull
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusDeviceEnum status;
}
