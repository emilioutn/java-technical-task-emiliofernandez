package com.example.deviceservice.model;

import com.example.deviceservice.enums.StatusDeviceEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "message")
public class MessageDocument {
    private UUID id;

    private Integer idDevice;

    @Enumerated(EnumType.STRING)
    private StatusDeviceEnum status;
}
