package com.example.deviceservice.builder;

import com.example.deviceservice.dto.MessageDto;
import com.example.deviceservice.event.bean.Message;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class MessageBuilder {
    public Message buildMessageFromDto(MessageDto messageDto) {
        return Message.builder()
                .id(messageDto.getId())
                .status(messageDto.getStatus())
                .build();
    }
}
