package com.example.deviceservice.dto;

import com.example.deviceservice.enums.StatusDeviceEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDto {
    private Integer id;
    private String name;

    @Enumerated(EnumType.STRING)
    private StatusDeviceEnum status;
}
