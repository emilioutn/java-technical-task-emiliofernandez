package com.example.deviceservice.rest.bean.status;

import com.example.deviceservice.dto.PushStatusDto;

public interface IStatusFeign {
    public void notifyUser(PushStatusDto pushStatusDto, Integer deviceId);
}
