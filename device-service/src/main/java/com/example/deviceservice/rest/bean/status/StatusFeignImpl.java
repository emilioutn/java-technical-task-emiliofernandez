package com.example.deviceservice.rest.bean.status;

import com.example.deviceservice.dto.PushStatusDto;
import com.example.deviceservice.rest.client.status.StatusClientRest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatusFeignImpl implements IStatusFeign {
    @Autowired
    private StatusClientRest statusFeign;

    @Override
    public void notifyUser(PushStatusDto pushStatusDto, Integer deviceId) {
        statusFeign.notifyUser(pushStatusDto, deviceId);
    }
}
