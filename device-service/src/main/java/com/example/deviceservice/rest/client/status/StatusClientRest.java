package com.example.deviceservice.rest.client.status;

import com.example.deviceservice.dto.PushStatusDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "status-service")
public interface StatusClientRest {
    @PostMapping(value = "/devices/{id}")
    public void notifyUser(@RequestBody PushStatusDto pushStatusDto, @PathVariable Integer id);
}
