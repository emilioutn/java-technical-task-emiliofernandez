package com.example.deviceservice.service.consumer;

import com.example.deviceservice.event.bean.Message;

public interface IConsumerMessageService {
    public void saveMessage(Message message);
}
