package com.example.deviceservice.service.consumer;

import com.example.deviceservice.event.bean.Message;
import com.example.deviceservice.model.MessageDocument;
import com.example.deviceservice.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ConsumerMessageServiceImpl implements IConsumerMessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public void saveMessage(Message message) {
        messageRepository.save(
                MessageDocument.builder()
                .id(UUID.randomUUID())
                .idDevice(message.getId())
                .status(message.getStatus())
                .build()
        );
    }
}
