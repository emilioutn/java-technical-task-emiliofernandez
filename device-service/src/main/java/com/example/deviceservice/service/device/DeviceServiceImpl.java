package com.example.deviceservice.service.device;

import com.example.deviceservice.dto.PushStatusDto;
import com.example.deviceservice.enums.StatusDeviceEnum;
import com.example.deviceservice.event.bean.Message;
import com.example.deviceservice.exception.NullMessageException;
import com.example.deviceservice.exception.StatusNotDefinedException;
import com.example.deviceservice.model.DeviceEntity;
import com.example.deviceservice.repository.DeviceRepository;
import com.example.deviceservice.rest.bean.status.IStatusFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DeviceServiceImpl implements IDeviceService {

    private final String STATUS_UP = "UP";
    private final String STATUS_DOWN = "DOWN";
    private final String STATUS_ALERTED = "ALERTED";

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private IStatusFeign statusFeign;

    @Override
    public Boolean alertAndUpdateDevice(Message message) throws StatusNotDefinedException {
        if (changeStatusDevice(Optional.ofNullable(message).orElseThrow(NullMessageException::new))) {
            notifyStatus(message);
            return true;
        }
        return false;
    }

    private Boolean changeStatusDevice(Message message) throws StatusNotDefinedException {
        DeviceEntity deviceDb = deviceRepository.findById(message.getId()).orElse(null);
        if (deviceDb != null) {
            StatusDeviceEnum newStatus = message.getStatus();
            Boolean notify = checkChangeStatusDeviceOk(deviceDb.getStatus().name(), newStatus.name());
            deviceDb.setStatus(newStatus);
            deviceRepository.save(deviceDb);
            return notify;
        }
        return false;
    }

    private Boolean checkChangeStatusDeviceOk(String oldStatus, String newStatus) throws StatusNotDefinedException {
        Boolean changeStatus = false;
        switch (oldStatus) {
            case STATUS_UP:
            case STATUS_DOWN:
                if (newStatus.equals(STATUS_ALERTED)) {
                    changeStatus = true;
                }
                break;
            case STATUS_ALERTED:
                if (newStatus.equals(STATUS_DOWN) || newStatus.equals(STATUS_UP)) {
                    changeStatus = true;
                }
                break;
        }
        return changeStatus;
    }

    private void notifyStatus(Message message) {
        statusFeign.notifyUser(PushStatusDto.builder().status(message.getStatus().name()).build(), message.getId());
    }
}
