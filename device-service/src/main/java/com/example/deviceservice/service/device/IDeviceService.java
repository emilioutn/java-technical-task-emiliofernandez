package com.example.deviceservice.service.device;

import com.example.deviceservice.event.bean.Message;
import com.example.deviceservice.exception.StatusNotDefinedException;

public interface IDeviceService {
    public Boolean alertAndUpdateDevice(Message message) throws StatusNotDefinedException;
}
