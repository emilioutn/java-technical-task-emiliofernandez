package com.example.deviceservice.repository;

import com.example.deviceservice.model.DeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends JpaRepository<DeviceEntity, Integer> {

}
