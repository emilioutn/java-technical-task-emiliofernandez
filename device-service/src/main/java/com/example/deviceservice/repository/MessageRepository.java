package com.example.deviceservice.repository;

import com.example.deviceservice.model.MessageDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MessageRepository extends MongoRepository<MessageDocument, Integer> {

}
