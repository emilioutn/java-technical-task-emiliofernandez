package com.example.deviceservice.event.bean;

import com.example.deviceservice.enums.StatusDeviceEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Message {
    private Integer id;

    @Enumerated(EnumType.STRING)
    private StatusDeviceEnum status;
}
