package com.example.deviceservice.event.consumer;

import com.example.deviceservice.builder.MessageBuilder;
import com.example.deviceservice.dto.MessageDto;
import com.example.deviceservice.event.bean.Message;
import com.example.deviceservice.event.producer.Producer;
import com.example.deviceservice.exception.StatusNotDefinedException;
import com.example.deviceservice.service.consumer.IConsumerMessageService;
import com.example.deviceservice.service.device.IDeviceService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MessageProcessConsumer {
    @Autowired
    private MessageBuilder messageBuilder;

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private IConsumerMessageService consumerMessageService;

    @Autowired
    private Producer producer;

    @Value("${spring.kafka.topics.push-notifications}")
    private String PUSH_NOTIFICATIONS_TOPIC;

    @KafkaListener(topics = "${spring.kafka.topics.external-topic}")
    public void consume(String message) {
        log.info("#### -> Consumed message {}", message);
        MessageDto messageDto;
        try {
            messageDto =  processMessageStr(message);
        } catch (JsonSyntaxException e) {
            log.error("Malformed message");
            return;
        }

        if(messageDto.getStatus() == null) {
            log.error("Status does not defined");
            return;
        }

        Message messageObj = messageBuilder.buildMessageFromDto(messageDto);

        pushNotification(message);

        consumerMessageService.saveMessage(messageObj);
        try {
            Boolean deviceAlerted = deviceService.alertAndUpdateDevice(messageObj);
            log.info("Device Alerted: {}", deviceAlerted);
        } catch(StatusNotDefinedException e) {
            log.error("Error consuming Message: {}. ******* Error: {}", message, e.getMessage());
        } catch (Exception e) {
            log.error("Unexpected Error: {}", e.getMessage());
        }
    }

    private MessageDto processMessageStr(String message) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        return gson.fromJson(message, MessageDto.class);
    }

    private void pushNotification(String message) {
        producer.sendMessage(PUSH_NOTIFICATIONS_TOPIC, message);
    }
}
