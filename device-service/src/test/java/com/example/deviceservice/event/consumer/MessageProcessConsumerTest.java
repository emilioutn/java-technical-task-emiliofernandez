package com.example.deviceservice.event.consumer;

import com.example.deviceservice.builder.MessageBuilder;
import com.example.deviceservice.dto.MessageDto;
import com.example.deviceservice.enums.StatusDeviceEnum;
import com.example.deviceservice.event.bean.Message;
import com.example.deviceservice.event.producer.Producer;
import com.example.deviceservice.exception.NullMessageException;
import com.example.deviceservice.exception.StatusNotDefinedException;
import com.example.deviceservice.service.consumer.IConsumerMessageService;
import com.example.deviceservice.service.device.IDeviceService;
import com.google.gson.JsonSyntaxException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
public class MessageProcessConsumerTest {
    @InjectMocks
    private MessageProcessConsumer messageProcessConsumer;

    @Mock
    private MessageProcessConsumer messageProcessConsumerMock;

    @Mock
    private MessageBuilder messageBuilder;

    @Mock
    private IConsumerMessageService consumerMessageService;

    @Mock
    private Producer producer;

    @Mock
    private IDeviceService deviceService;

    @Test
    public void consume_whenReceivedMessageWithProducer_thenPushMessage()
            throws StatusNotDefinedException {

        String messageReceiverStr = "{'id': 1, 'status': 'ALERTED'}";
        Message message = Message.builder().id(1).status(StatusDeviceEnum.ALERTED).build();

        when(messageBuilder.buildMessageFromDto(any(MessageDto.class))).thenReturn(message);
        doNothing().when(producer).sendMessage(anyString(), anyString());
        doNothing().when(consumerMessageService).saveMessage(any(Message.class));
        when(deviceService.alertAndUpdateDevice(any(Message.class))).thenReturn(anyBoolean());
        messageProcessConsumer.consume(messageReceiverStr);
    }

    @Test
    public void consume_whenReceivedMessageMalformed_thenLogErrorForJsonSyntaxException() {
        String messageReceiverStr = "asdf";
        doThrow(new RuntimeException()).when(messageProcessConsumerMock).consume(messageReceiverStr);
        messageProcessConsumer.consume(messageReceiverStr);
    }

    @Test
    public void consume_whenReceivedMessageWithNotExistStatus_thenLogAndEnd() {
        String messageReceiverStr = "{'id': 1, 'status': 'OTHER'}";
        messageProcessConsumer.consume(messageReceiverStr);
    }
}
