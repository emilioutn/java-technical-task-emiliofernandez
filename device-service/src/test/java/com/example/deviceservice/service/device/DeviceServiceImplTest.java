package com.example.deviceservice.service.device;

import com.example.deviceservice.dto.PushStatusDto;
import com.example.deviceservice.enums.StatusDeviceEnum;
import com.example.deviceservice.event.bean.Message;
import com.example.deviceservice.exception.NullMessageException;
import com.example.deviceservice.exception.StatusNotDefinedException;
import com.example.deviceservice.model.DeviceEntity;
import com.example.deviceservice.repository.DeviceRepository;
import com.example.deviceservice.rest.bean.status.IStatusFeign;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DeviceServiceImplTest {

    @InjectMocks
    private DeviceServiceImpl deviceService;

    @Mock
    private DeviceRepository deviceRepository;

    @Mock
    private IStatusFeign statusFeign;

    private DeviceEntity deviceOfDb;

    @Before
    public void init() {
        deviceOfDb = creteDefaultDevice();

        when(deviceRepository.findById(1)).thenReturn(Optional.of(deviceOfDb));
        when(deviceRepository.save(any(DeviceEntity.class))).thenReturn(deviceOfDb);
        doNothing().when(statusFeign).notifyUser(any(PushStatusDto.class), anyInt());
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveAlertedStatusAndOldStatusIsUp_thenSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.ALERTED);
        deviceOfDb.setStatus(StatusDeviceEnum.UP);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertTrue(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveAlertedStatusAndOldStatusIsDown_thenSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.ALERTED);
        deviceOfDb.setStatus(StatusDeviceEnum.DOWN);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertTrue(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveAlertedStatusAndOldStatusIsAlerted_thenNotSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.ALERTED);
        deviceOfDb.setStatus(StatusDeviceEnum.ALERTED);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertFalse(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveDownStatusAndOldStatusIsAlerted_thenSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.DOWN);
        deviceOfDb.setStatus(StatusDeviceEnum.ALERTED);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertTrue(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveDownStatusAndOldStatusIsUp_thenNotSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.DOWN);
        deviceOfDb.setStatus(StatusDeviceEnum.UP);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertFalse(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveDownStatusAndOldStatusIsDown_thenNotSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.DOWN);
        deviceOfDb.setStatus(StatusDeviceEnum.DOWN);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertFalse(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveUpStatusAndOldStatusIsAlerted_thenSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.UP);
        deviceOfDb.setStatus(StatusDeviceEnum.ALERTED);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertTrue(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveUpStatusAndOldStatusIsDown_thenNotSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.UP);
        deviceOfDb.setStatus(StatusDeviceEnum.DOWN);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertFalse(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveUpStatusAndOldStatusIsUp_thenNotSendPush() throws StatusNotDefinedException {
        Message topicMessage = createReceivedMessage(StatusDeviceEnum.UP);
        deviceOfDb.setStatus(StatusDeviceEnum.UP);

        Boolean statusChange = deviceService.alertAndUpdateDevice(topicMessage);

        assertEquals(deviceOfDb.getStatus(), topicMessage.getStatus());
        assertFalse(statusChange);
    }

    @Test
    public void alertAndUpdateDevice_whenReceiveNullMessage_thenThrowNullMessageException() {
        assertThatThrownBy(() -> deviceService.alertAndUpdateDevice(null)).isInstanceOf(NullMessageException.class);
    }

    private DeviceEntity creteDefaultDevice() {
        return DeviceEntity.builder()
                .id(1)
                .name("Samsung S22")
                .status(StatusDeviceEnum.DOWN)
                .build();
    }

    private Message createReceivedMessage(StatusDeviceEnum statusDeviceEnum) {
        return Message.builder().id(1).status(statusDeviceEnum).build();
    }

}



