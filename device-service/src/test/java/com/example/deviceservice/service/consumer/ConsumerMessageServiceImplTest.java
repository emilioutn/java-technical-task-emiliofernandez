package com.example.deviceservice.service.consumer;

import com.example.deviceservice.enums.StatusDeviceEnum;
import com.example.deviceservice.event.bean.Message;
import com.example.deviceservice.model.MessageDocument;
import com.example.deviceservice.repository.MessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ConsumerMessageServiceImplTest {

    @InjectMocks
    private ConsumerMessageServiceImpl consumerMessageService;

    @Mock
    private MessageRepository messageRepository;

    @Test
    public void saveMessage_whenReciveMessage_thenSaveMessageToMongodb() {
        Message topicMessage = Message.builder().id(1).status(StatusDeviceEnum.DOWN).build();
        MessageDocument messageDocument = createMessageDocument(topicMessage);
        when(messageRepository.save(any(MessageDocument.class))).thenReturn(messageDocument);
        consumerMessageService.saveMessage(topicMessage);
    }

    private MessageDocument createMessageDocument(Message message) {
        return MessageDocument.builder()
                .id(UUID.randomUUID())
                .idDevice(message.getId())
                .status(message.getStatus())
                .build();
    }
}
